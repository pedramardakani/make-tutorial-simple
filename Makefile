# This file is produced as a part of an educational course.
#
# Create a pie plot. Read the data from 'data.csv' file. Plot the pie
# chart using 'matplotlib' package. The python code is taken from the
# official matplotlib examples page [1], and slightly altered to read
# the inputs from the 'data.csv' file.
#
# Copyright (C) 2021 Pedram Ashofteh Ardakani <pedramardakani@pm.me>
#
# [1] https://matplotlib.org/devdocs/gallery/pie_and_polar_charts/pie_features.html#sphx-glr-gallery-pie-and-polar-charts-pie-features-py
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this file. If not, see <http://www.gnu.org/licenses/>.

# Set the default shell environment
SHELL = /bin/bash

# Make settings
all: out.png





# Create the output pie plot using the python program
out.png: pie.py data.csv
	python3 pie.py




# House-keeping
clean:; rm -fv out.png
