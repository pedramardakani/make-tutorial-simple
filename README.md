Simple Makefile demonstration
-----------------------------

Create a simple *pie plot*. Read the data from 'data.csv' file. Plot
the pie-chart using 'matplotlib' package. The python code is taken
from the official matplotlib examples page, and slightly altered to
read the inputs from the 'data.csv' file.

To produce the example 'out.png' file, simply run `make`.
