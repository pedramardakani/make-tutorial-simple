# This file is produced as a part of an educational course.
#
# Create a pie plot. Read the data from 'data.csv' file. Plot the pie
# chart using 'matplotlib' package. The python code is taken from the
# official matplotlib examples page [1], and slightly altered to read
# the inputs from the 'data.csv' file.
#
# Copyright (C) 2021 Pedram Ashofteh Ardakani <pedramardakani@pm.me>
#
# [1] https://matplotlib.org/devdocs/gallery/pie_and_polar_charts/pie_features.html#sphx-glr-gallery-pie-and-polar-charts-pie-features-py
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this file. If not, see <http://www.gnu.org/licenses/>.

from csv import reader
import matplotlib.pyplot as plt

# Pie chart, where the slices will be ordered and plotted counter-clockwise:

labels, sizes = [], []
with open('data.csv', 'r') as f:
    read = reader(f)
    for line in read:
        name, value = line
        labels.append(name)
        sizes.append(float(value))

# ORIGINAL EXAMPLE:
# sizes = [15, 30, 45, 10]
# explode = (0, 0.1, 0, 0)
# labels = 'Frogs', 'Hogs', 'Dogs', 'Logs'

explode = None
fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=90)
# Equal aspect ratio ensures that pie is drawn as a circle.
ax1.axis('equal')

#plt.show()
plt.savefig('out.png')
